//
//  HostingController.swift
//  WatchAppBasic WatchKit Extension
//
//  Created by Giovanni Gorgone on 14/04/2020.
//  Copyright © 2020 ggorgone. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
