//
//  NotificationView.swift
//  WatchAppBasic WatchKit Extension
//
//  Created by Giovanni Gorgone on 14/04/2020.
//  Copyright © 2020 ggorgone. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
